import { Component } from 'react'
import TeardropCanvas from './teardrop-canvas'

class ImageCanvas extends Component {
	constructor(props) {
		super(props)

		this.state = {
			imgStart: [0, 0],
			imgPosition: [0, 0],
			isDragging: false,
			dragStart: [0, 0],
			imgWidth: null,
			scale: 1
		}

		this.onImageStartDrag = this.onImageStartDrag.bind(this)
		this.onImageDrag = this.onImageDrag.bind(this)
		this.onImageEndDrag = this.onImageEndDrag.bind(this)
	}

	onImageStartDrag(e) {
		if (!this.props.allowDrag) return

		this.setState({
			isDragging: true,
			dragStart: [
				e.touches[0].clientX,
				e.touches[0].clientY
			],
		})
	}

	onImageEndDrag(e) {
		if (!this.state.isDragging) return

		this.setState({
			isDragging: false,
			imgStart: this.state.imgPosition
		})
	}

	onImageDrag(e) {
		if (!this.state.isDragging) return

		const touch = e.touches[0]
		const delta = [
			touch.clientX - this.state.dragStart[0],
			touch.clientY - this.state.dragStart[1],
		]

		this.setState({
			imgPosition: [
				this.state.imgStart[0] + delta[0],
				this.state.imgStart[1] + delta[1],
			]
		})
	}

	onImageLoad(e) {
		this.setState({
			imgWidth: e.target.clientWidth
		})

		this.props.onLoad(e.target.clientWidth, e.target.clientHeight)
	}

	componentDidMount() {
		window.addEventListener('resize', () => {
			const scale = (this.imgEle.clientWidth) / this.state.imgWidth * this.state.scale
			this.setState({
				imgWidth: this.imgEle.clientWidth,
				scale
			})
		})
	}

	render() {
		const drawingProps =
			this.props.allowDrag ?
			{
				onTouchStart: this.onImageStartDrag,
				onTouchMove: this.onImageDrag,
				onTouchEnd: this.onImageEndDrag
			} :
			{}

		const scale = this.state.scale * this.props.zoom

		return (
			<div className="root">
				<div className="img-wrapper">
					<div className="drawing"
						{...drawingProps}>
						<img
							className={this.props.invert ? 'inverted' : 'normal'}
							src={this.props.src}
							ref={el => this.imgEle = el}
							onLoad={this.onImageLoad.bind(this)}
							style={{ width: `${100 * this.props.zoom}%` }}/>
						<div className="points">
							{
								this.state.imgWidth &&
								this.props.showPoints &&
								<TeardropCanvas
									allowDrag={!this.props.allowDrag}
									points={this.props.points || []}
									onUpdatePoints={this.props.onDropChange}
									offset={this.state.imgPosition}
									referenceWidth={this.state.imgWidth * this.props.zoom}
									angle={this.props.angle}
									angleDirection={this.props.angleDirection}
									scale={this.state.scale}
									zoom={this.props.zoom}
									width={`${100 * this.props.zoom}%`}/>
							}
						</div>
					</div>
				</div>
				<style jsx>{`
					.root {
						width: 100%;
						height: 100%;
						position: relative;
					}

					.img-wrapper {
						width: 100%;
						height: 100%;
						overflow: hidden;
						position: relative;
					}

					.img-wrapper > .drawing {
						position: absolute;
						left: ${this.state.imgPosition[0]}px;
						top: ${this.state.imgPosition[1]}px;
						width: 100%;
						height: 100%;
					}

					.points {
						position: absolute;
						width: 100%;
						height: 100%;
						top: 0;
						left: 0;
					}

					.points > div {
						width: 20px;
						height: 20px;
						background: tomato;
					}

					img.normal {
						filter: grayscale(1);
					}

					img.inverted {
						filter: grayscale(1) invert(1);
					}
				`}</style>
			</div>
		)
	}
}

export default ImageCanvas
