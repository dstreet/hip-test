import { Camera, Move, Droplet, RotateCcw } from 'react-feather'
import IconButton from 'material-ui/IconButton'
import TextField from 'material-ui/TextField'
import Exposure from 'material-ui/svg-icons/image/exposure'

const colors = {
	inactive: '#666',
	active: '#fff'
}

const Toolbar = ({ onClickCamera, onClickDrag, onClickDrop, onClickFlip, onClickInvert, activeItems, angle, onAngleChange }) => {
	return (
		<div className="root">
			<TextField hintText="Inklination angle" type="number" value={angle} onChange={e => onAngleChange(e.target.value)}/>
			<div className="icon-group">
				<IconButton onClick={onClickCamera}>
					<Camera size={30} color={colors.inactive}/></IconButton>
				<IconButton onClick={onClickDrag}>
					<Move size={30} color={activeItems.indexOf('IMAGE_DRAG') > -1 ? colors.active : colors.inactive}/>
				</IconButton>
				<IconButton onClick={onClickDrop}>
					<Droplet size={30} color={activeItems.indexOf('VIEW_DROPS') > -1 ? colors.active : colors.inactive}/>
				</IconButton>
				<IconButton onClick={onClickFlip}>
					<RotateCcw size={30} color={colors.inactive}/>
				</IconButton>
				<IconButton onClick={onClickInvert}>
					<Exposure color={colors.inactive}/>
				</IconButton>
			</div>

			<style jsx>{`
				.root {
					background-color: rgba(0, 0, 0, 0.8);
					padding: 0 16px 10px 16px;
					display: flex;
				}
				.icon-group {
					text-align: right;
					flex-basis: 100%;
					flex: 1 0;
				}
			`}</style>
		</div>
	)
}

export default Toolbar
