import { getSlope } from '../utils'

const TeardropLine = ({ points, width }) => {
	if (points.length < 2) {
		return null
	}

	const slope = getSlope(points[0], points[1])
	const lineStart = [ 0, (points[0][0] * slope - points[0][1]) * -1 ]
	const lineEnd = [ width, ((width - points[1][0]) * slope + points[1][1]) ]

	return (
		<line
			x1={lineStart[0]}
			x2={lineEnd[0]}
			y1={lineStart[1]}
			y2={lineEnd[1]}
			stroke="cyan"
			strokeWidth={2}/>
	)
}

export default TeardropLine
