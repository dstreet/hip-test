import { Component } from 'react'
import TeardropPoints from './teardrop-points'
import TeardropLine from './teardrop-line'
import InklinationLine from './inklination-line'

import { getSlope } from '../utils'

class TeardropCanvas extends Component {
	constructor(props) {
		super(props)

		this.state = {
			isDraggingPoint: null,
			isDraggingLine: false,
			lineDragStartX: null,
			lineOffset: 0
		}

		this.onPointDragStart = this.onPointDragStart.bind(this)
		this.onPointDrag = this.onPointDrag.bind(this)
		this.onPointDragEnd = this.onPointDragEnd.bind(this)
		this.onLineDragStart = this.onLineDragStart.bind(this)
		this.onLineDrag = this.onLineDrag.bind(this)
		this.onLineDragEnd = this.onLineDragEnd.bind(this)
	}

	onPointDragStart(index, e) {
		if (!this.props.allowDrag) return

		this.setState({
			isDraggingPoint: index
		})
	}

	onPointDrag(index, e) {
		if (this.state.isDraggingPoint === null) return

		const touch = e.touches[0]
		const tmpPoints = [...this.props.points]

		tmpPoints[index] = [
			touch.clientX - this.props.offset[0],
			touch.clientY - this.props.offset[1]
		]

		this.props.onUpdatePoints(tmpPoints)
	}

	onPointDragEnd(index, e) {
		if (!this.state.isDraggingPoint === null) return

		this.setState({
			isDraggingPoint: null
		})
	}

	onLineDragStart(startX, e) {
		if (!this.props.allowDrag) return

		const touch = e.touches[0]

		this.setState({
			isDraggingLine: true,
			lineDragStartX: touch.clientX,
			lineOffsetStart: this.state.lineOffset
		})
	}

	onLineDrag(e) {
		if (!this.state.isDraggingLine) return

		const touch = e.touches[0]
		const deltaX = touch.clientX - this.state.lineDragStartX
		
		this.setState({
			lineOffset: this.state.lineOffsetStart + deltaX
		})
	}

	onLineDragEnd(e) {
		if (!this.state.isDraggingLine) return

		this.setState({
			isDraggingLine: null
		})
	}

	render() {
		const { referenceWidth, angle } = this.props
		const points = this.props.points.map(point => {
			return [
				point[0] * this.props.scale * this.props.zoom,
				point[1] * this.props.scale * this.props.zoom
			]
		})

		return (
			<svg className="root" ref={el => this.rootElement = el}>
				<TeardropLine
					points={points}
					width={referenceWidth}/>
				<TeardropPoints
					points={points}
					onPointTouchStart={this.onPointDragStart}
					onPointTouchMove={this.onPointDrag}
					onPointTouchEnd={this.onPointDragEnd}/>
				<InklinationLine
					points={points}
					width={referenceWidth}
					angle={angle}
					direction={this.props.angleDirection}
					offset={this.state.lineOffset}
					onLineTouchStart={this.onLineDragStart}
					onLineTouchMove={this.onLineDrag}
					onLineTouchEnd={this.onLineDragEnd}/>
				<style jsx>{`
					.root {
						width: ${100 * this.props.zoom}%;
						height: ${100 * this.props.zoom}%;
						opacity: 0.5;
					}
				`}</style>
			</svg>
		)
	}
}

export default TeardropCanvas
