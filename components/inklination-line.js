import { getSlope, getY } from '../utils'

const InklinationLine = ({ points, angle, width, offset, direction = 1, onLineTouchStart, onLineTouchMove, onLineTouchEnd }) => {
	if (points.length < 2 || !angle) {
		return null
	}

	const angleInt = parseInt(angle, 10)

	const baseSlope = getSlope(points[0], points[1])
	const baseAngle = Math.atan(Math.abs(baseSlope)) * (180 / Math.PI)
	const totalAngle = angleInt + ( baseAngle * (baseSlope < 0 ? direction : -direction) )
	const angleLineSlope = Math.tan(totalAngle * Math.PI / 180) * -direction
	
	const startX = width / 2 + offset
	const start = [ startX, getY(baseSlope, points[0], startX) ]

	const endX = direction > 0 ? width : 0
	const end = [
		endX,
		getY(angleLineSlope, start, endX)
	]

	return [
		<line
			key="line"
			x1={start[0]}
			x2={end[0]}
			y1={start[1]}
			y2={end[1]}
			stroke="magenta"
			strokeWidth={2}/>,
		<circle
			key="circle"
			cx={start[0]}
			cy={start[1]}
			r={10}
			fill="magenta"
			onTouchStart={e => onLineTouchStart(startX, e)}
			onTouchMove={onLineTouchMove}
			onTouchEnd={onLineTouchEnd}/>
	]
}

export default InklinationLine
