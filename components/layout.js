import { Component } from 'react'
import Head from 'next/head'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

export default class Home extends Component {
	componentDidMount() {
		// document.addEventListener('touchmove', e => e.preventDefault())
	}

	render() {
		const muiTheme = getMuiTheme(darkBaseTheme, {
			userAgent: this.props.userAgent
		})

		return (
			<div>
				<Head>
					<title>HipTest</title>
					<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0"/>
					<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet"/>
				</Head>
				
				<MuiThemeProvider muiTheme={muiTheme}>
					<div className="page-wrapper">
						{this.props.children}
					</div>
				</MuiThemeProvider>

				<style jsx global>{`
					html, body { margin: 0 }
				`}</style>

				<style jsx>{`
					.page-wrapper {
						position: fixed;
						width: 100%;
						height: 100%;
						background-color: #141414;
						color: #c6c6c6;
						font-family: 'Roboto';
						font-size: 16px;
						margin: 0;
					}}
				`}</style>
			</div>
		)
	}
}
