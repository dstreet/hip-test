const TeardropPoints = ({ points, onPointTouchStart, onPointTouchMove, onPointTouchEnd }) => {
	function onTouchEvent(index, e) {
		switch (e.type) {
			case 'touchstart':
				onPointTouchStart(index, e)
				break
			case 'touchmove':
				onPointTouchMove(index, e)
				break
			case 'touchend':
				onPointTouchEnd(index, e)
		}
	}
	
	const circles = points.map((point, i) => (
		<circle
			key={`point-${i}`}
			cx={point[0]}
			cy={point[1]}
			r={10}
			fill="cyan"
			onTouchStart={onTouchEvent.bind(null, i)}
			onTouchMove={onTouchEvent.bind(null, i)}
			onTouchEnd={onTouchEvent.bind(null, i)}/>
	))

	return circles
}

export default TeardropPoints
