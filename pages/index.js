import { Component } from 'react'
import { connect } from '../store'
import { setImage, addInteraction, removeInteraction, addPoint, updatePoints, setInklinationAngle, setInklinationAngleDirection, invertImage } from '../store/actions'
import Layout from '../components/layout'
import RaisedButton from 'material-ui/RaisedButton'
import IconButton from 'material-ui/IconButton'
import { Camera, Move } from 'react-feather'
import ImageCanvas from '../components/image-canvas'
import Toolbar from '../components/toolbar'

class Home extends Component {
	static getInitialProps({ req }) {
		return { userAgent: req.headers['user-agent'] }
	}

	constructor(props) {
		super(props)

		this.onNewImage = this.onNewImage.bind(this)
		this.onPictureButton = this.onPictureButton.bind(this)
		this.onDropChange = this.onDropChange.bind(this)
		this.onAngleChange = this.onAngleChange.bind(this)
		this.onCanvasLoad = this.onCanvasLoad.bind(this)
	}

	onCanvasLoad(width, height) {

		if (!this.props.image.points) {
			this.props.dispatch(addPoint(width - 20, height / 2))
			this.props.dispatch(addPoint(20, height / 2))
		}
	}

	onPictureButton() {
		this.imageInput.dispatchEvent(new MouseEvent('click'))
	}

	onNewImage(e) {
		const file = e.target.files[0]

		if (!file) return
		this.props.dispatch(setImage(URL.createObjectURL(file)))
	}

	onToolbarClick(type) {
		const points = this.props.image.points || []
		let action

		switch (type) {
			case 'camera':
				this.onPictureButton()
			case 'drag':
				action = this.props.interaction.enabled.indexOf('IMAGE_DRAG') > -1 ? removeInteraction : addInteraction
				
				this.props.dispatch(
					action.call(null, 'IMAGE_DRAG')
				)
				break
			case 'drop':
				action = this.props.interaction.enabled.indexOf('VIEW_DROPS') > -1 ? removeInteraction : addInteraction

				this.props.dispatch(
					action.call(null, 'VIEW_DROPS')
				)
				break
			case 'flip':
				this.props.dispatch(
					setInklinationAngleDirection(this.props.image.inklinationAngleDirection * -1)
				)
				break
			case 'invert':
				this.props.dispatch(
					invertImage(!this.props.image.invert)
				)
				break
			default:
				break;
		}
	}

	onDropChange(points) {
		this.props.dispatch(updatePoints(points))
	}

	onAngleChange(angle) {
		this.props.dispatch(setInklinationAngle(angle))
	}

	render() {
		const { userAgent, image, interaction } = this.props
		let activeToolbarItem

		switch (interaction.enabled) {
			case 'IMAGE_DRAG':
				activeToolbarItem = 'drag'
				break
			case 'ADD_DROP':
				activeToolbarItem = 'drop'
				break
			default:
				break
		}

		return (
			<Layout userAgent={userAgent}>
				<input
					className="file-input"
					type="file"
					accept="image/*"
					capture
					onChange={this.onNewImage}
					ref={el => this.imageInput = el}/>
				<div className="root">
					{
						!image.hasImage &&
						<div className="get-started">
							<div className="title">Let's get started</div>
							<RaisedButton primary label="Take a picture" onClick={this.onPictureButton}/>
						</div>
					}
					{
						image.hasImage &&
						<div className="toolbar-wrapper">
							<Toolbar
								onClickCamera={this.onToolbarClick.bind(this, 'camera')}
								onClickDrag={this.onToolbarClick.bind(this, 'drag')}
								onClickDrop={this.onToolbarClick.bind(this, 'drop')}
								onClickFlip={this.onToolbarClick.bind(this, 'flip')}
								onClickInvert={this.onToolbarClick.bind(this, 'invert')}
								activeItems={interaction.enabled}
								angle={image.inklinationAngle}
								onAngleChange={this.onAngleChange}/>
						</div>
					}
					{
						image.hasImage &&
						<ImageCanvas
							onLoad={this.onCanvasLoad}
							src={image.src}
							invert={image.invert}
							allowDrag={interaction.enabled.indexOf('IMAGE_DRAG') + 1}
							points={image.points}
							showPoints={interaction.enabled.indexOf('VIEW_DROPS') + 1}
							onDropChange={this.onDropChange}
							angle={image.inklinationAngle}
							angleDirection={image.inklinationAngleDirection}
							zoom={image.zoom}/>
					}
				</div>
				<style jsx>{`
					.file-input {
						display: none;
					}

					.root {
						display: flex;
						flex-direction: column;
						align-items: center;
						justify-content: center;
						height: 100%;
					}

					.get-started {
						text-align: center;
					}

					.get-started .title {
						font-size: 24px;
						margin: 24px 0;
					}

					.toolbar-wrapper {
						width: 100%;
						position: absolute;
						top: 0;
						z-index: 999;
					}
				`}</style>
			</Layout>
		)
	}
}

export default connect(state => state)(Home)
