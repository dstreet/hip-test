export const setImage = src => dispatch => {
	dispatch({ type: 'SET_IMAGE', data: src })
}

export const addInteraction = type => dispatch => {
	dispatch({ type: 'ADD_INTERACTION', data: type })
}

export const removeInteraction = type => dispatch => {
	dispatch({ type: 'REMOVE_INTERACTION', data: type })
}

export const addPoint = (x, y) => dispatch => {
	dispatch({ type: 'ADD_POINT', data: [x, y] })
}

export const updatePoints = points => dispatch => {
	dispatch({ type: 'UPDATE_POINTS', data: points })
}

export const setInklinationAngle = angle => dispatch => {
	dispatch({ type: 'SET_INKLINATION_ANGLE', data: angle })
}

export const setInklinationAngleDirection = angle => dispatch => {
	dispatch({ type: 'SET_INKLINATION_ANGLE_DIRECTION', data: angle })
}

export const invertImage = invert => dispatch => {
	dispatch({ type: 'SET_IMAGE_INVERT', data: invert })
}
