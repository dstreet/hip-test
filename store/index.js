import { createStore, applyMiddleware, compose } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import nextConnectRedux from 'next-connect-redux'
import reducer from './reducer'

export const initStore = (initialState = {
	image: {
		inklinationAngle: 45,
		inklinationAngleDirection: 1,
		zoom: 1
	},
	interaction: {
		enabled: [ 'VIEW_DROPS' ]
	}
}) => {
	const composeEnhancers = typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
	
	return createStore(
		reducer,
		initialState,
		composeEnhancers(
			applyMiddleware(thunkMiddleware)
		)
	)
}

export const connect = nextConnectRedux(initStore)
