import { combineReducers } from 'redux'

function image(state = {}, action) {
	switch (action.type) {
		case 'SET_IMAGE':
			return {
				...state,
				hasImage: true,
				src: action.data
			}
		case 'ADD_POINT':
			const points = state.points || []
			return {
				...state,
				points: [...points, action.data]
			}
		case 'UPDATE_POINTS':
			return {
				...state,
				points: action.data
			}
		case 'SET_INKLINATION_ANGLE':
			return {
				...state,
				inklinationAngle: action.data
			}
		case 'SET_INKLINATION_ANGLE_DIRECTION':
			return {
				...state,
				inklinationAngleDirection: action.data
			}
		case 'SET_IMAGE_INVERT':
			return {
				...state,
				invert: action.data	
			}
		case 'SET_ZOOM_LEVEL':
			return {
				...state,
				zoom: action.data
			}
		default:
			return state
	}
}

function interaction(state = {}, action) {
	const enabled = state.enabled || []

	switch (action.type) {
		case 'ADD_INTERACTION':
			return {
				enabled: enabled.concat(action.data)
			}
		case 'REMOVE_INTERACTION':
			return {
				enabled: enabled.filter(item => item !== action.data)
			}
		default:
			return state
	}
}

export default combineReducers({
	image,
	interaction
})
