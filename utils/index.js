export function getSlope(a, b) {
	return (b[1] - a[1]) / (b[0] - a[0])
}

export function getY(slope, point, x) {
	return slope * (x - point[0]) + point[1]
}
